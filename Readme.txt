Link catre aplicatie: https://gitlab.upt.ro/denis.vetan/Licenta_Proiect_Serenity.git

- Se descarca sub format zipped.

--------------------------------------
- Pentru partea de aplicatie de Serenity Gallery avem două fișiere de tip zipped:
  'wordpress.sql.zip' si 'www.zip'
- Descarcam si instalam aplicatia 'WAMP'
- Stergem tot ce se afla in fisierul 'www' al aplicatiei 'WAMP'
- Dezarhivam fisierul 'www.zip' si introducem continutul acestuia in fisierul 'www'
- Deschidem browser-ul si in URL bar scriem 'localhost/phpMyAdmin'
- Ne autentificam cu username: 'root' si parola: ''
- Se creeaza o baza de date numita 'wordpress'
- Se importa fisierul zip in sectiuna de 'Import'
- Se poate rula aplicatia pe adresa de URL : 'localhost'
- ATENTIE! Pentru autentificarea in site 'localhost/login' se introduce username:'root', iar 
  parola 'root' 



--------------------------------------
- Pentru partea de aplicatie de Serenity Chat avem un fisier format zip:
  'chat-app.zip'
- Instalam Node.js: https://nodejs.org/en
- Instalam Visual Studio Code : https://code.visualstudio.com/
- Dezarhivam fisierul nostru 'chat-app'
- Deschidem Visual Studio Code
- In terminalul din Visual Studio code vom introduce comanda: npm install react-scripts
- ATENTIE! Aplicatie foloseste o cheie de API din firebase din contul meu deci poate va fi
  nevoie de o configurare noua (Solutii creearea unui cont al dumneavoastra de firebase si
  introducerea comenzii: npm install firebase)
- Introducem comanda cd urmat de calea folderului dezarhivat 'chat-app' si introducerea 
  acestuia in terminalul din Visual Studio Code.
- Introducem comanda: npm start, iar aplicatie trebuie sa ruleze.  